'use strict'

import axios from 'axios'

let Xhr = function () {
}

Xhr.install = function (Vue, options) {
    let http = axios.create({
        baseURL: 'http://apiklip.youverify.co/v1/'
    })

    http.interceptors.response.use(
        function (response) {
            return response.data
        },
        function (error) {
            // let data = error.response
            // let message = data.error

            // if (error.response.status === 422) {
            //     message = data[Object.keys(data)]
            // }

            // Vue.notify.error(error.response
            //     ? message
            //     : error)

            return Promise.reject(error)
        })

    let methods = ['post', 'get', 'delete']

    methods.forEach(method => {
        let a = http[method]
        http[method] = function (...args) {
            return new Promise(function (resolve, reject) {
                a(...args).then(resolve).catch(reject)
            })
        }
    })

    Vue.xhr = new Xhr(http)

    Vue.prototype.$http = http

    // Object.defineProperties(Vue.prototype, {
    //     $http: {
    //         get () {
    //             return http
    //         }
    //     }
    // })
}

export default Xhr