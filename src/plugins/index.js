'use strict';

import Vue from 'vue'
import Http from './http'

export default function () {
    Vue.use(Http);
}